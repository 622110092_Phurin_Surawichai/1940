using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class resetTarget : MonoBehaviour
{
    [SerializeField] GameObject startingV;

    public float minWait = 3;
    public float maxWait = 7;
    public float ResetForce=8000;
    //[SerializeField] GameObject startingT;
    //[SerializeField]Vector3 Down = new Vector3();

    Vector3 Up = new Vector3(0, 1, 0);
    //Vector3 Tup = new Vector3(0, 0, -1);
    float timer = 0;
    bool timerReached = false;
    // Start is called before the first frame update
    void Start()
    {
        
        
    }

    // Update is called once per frame
    private void OnTriggerEnter(Collider other)
    {
        
        Invoke("resetWait",Random.Range(minWait, maxWait));
        
    }
    void resetWait()
    {
        startingV.GetComponent<Rigidbody>().AddForce(Up * ResetForce, ForceMode.Impulse);
        if (!timerReached)
            timer += Time.deltaTime;
        if(!timerReached&&timer>1)
        {
            //startingV.transform.position = Down;
            startingV.transform.rotation = Quaternion.Euler(0, 0, 0);
            //startingT.GetComponent<Rigidbody>().AddForce(Tup*2000, ForceMode.Impulse);
            //timerReached = true;
        }
        
    }

}
