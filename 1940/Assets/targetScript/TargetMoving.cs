using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetMoving : MonoBehaviour
{
    private Vector3 movingtarget;
    public float movingSpeed;
    public float maxMove;
    public float minMove;
    // Start is called before the first frame update
    void Start()
    {
        movingtarget = new Vector3(movingSpeed, 0, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (this.transform.position.x > maxMove)
        {
            movingSpeed = movingSpeed * -1;
            movingtarget = new Vector3(movingSpeed, 0, 0);
        }
        if (this.transform.position.x < minMove)
        {
            movingSpeed = movingSpeed * -1;
            movingtarget = new Vector3(movingSpeed, 0, 0);
        }
        
        this.transform.position += movingtarget;
        
    }
    
}
