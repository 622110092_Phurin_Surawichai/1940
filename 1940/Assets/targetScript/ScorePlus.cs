using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ScorePlus : MonoBehaviour
{
    //[SerializeField] GameObject Traget;

    [SerializeField]  Text txtscore;
    public int scoreplus = 100;
    //public int scoreminus = 50;
     float timer;
    public int score;
    // Start is called before the first frame update
    void Start()
    {
        //txtscore.text = score.ToString();
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag=="Bullet")
        {
            if (timer > 3)
            {
                score += scoreplus;
                timer = 0;
            }
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        timer += Time.deltaTime;
        txtscore.text = score.ToString();
    }
}
