using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ScoreControl : MonoBehaviour
{
    //[SerializeField] GameObject Traget;
    [SerializeField]  Text txtscore;
    public int scoreplus = 100;
    float timer;
    static int score;
    // Start is called before the first frame update
    void Start()
    {
        score = 0;
        txtscore.text = score.ToString();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Bullet")
        {
            if (timer > 3)
            {
                score += scoreplus;
                timer = 0;
            }
        }
        if (score < 0)
        {
            score = 0;
        }
    }

    /// Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        txtscore.text = score.ToString();
    }
}
