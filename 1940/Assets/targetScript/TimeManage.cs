using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class TimeManage : MonoBehaviour
{

    
    
[SerializeField]  GameObject FPS;
[SerializeField]  Text txttime;
static float timer = 60;
float delaytime;
[SerializeField] Button BackButton;
[SerializeField] Button TryButton;
[SerializeField] GameObject panel;

private int convertTime;
// Start is called before the first frame update
void Start()
{
    timer = 60;
    txttime.text = timer.ToString();
    panel.SetActive(false);
    BackButton.onClick.AddListener (delegate{BackButtonClick(BackButton);});
    TryButton.onClick.AddListener (delegate{TryButtonClick(TryButton);});
}

private void OnTriggerEnter(Collider collider)
{
    
    if (delaytime > 3 && collider.tag =="Bullet")
        {
            timer += 5;
                
        }

}
// Update is called once per frame
void FixedUpdate()
{
    if (timer >= 0)
    {
        timer -= Time.deltaTime;
    }
    delaytime += Time.deltaTime;
    convertTime = (int) timer;
    txttime.text = convertTime.ToString();
    if (timer <= 0.02)
    {
        FPS.SetActive(false);
        timer = 0;
        panel.SetActive(true);
        
    }
    
}
public void BackButtonClick(Button button) 
{
    SceneManager.LoadScene("SceneMainMenu");
}
    
public void TryButtonClick(Button button)
{
    panel.SetActive(false);
    FPS.SetActive(true);
    SceneManager.LoadScene("gameplayIguess");
}
}