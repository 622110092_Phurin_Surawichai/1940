using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
public class DestroySound : MonoBehaviour
{
    [SerializeField] private GameObject Sound;

    public AudioSource SFX;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Bullet")
        {
            SFX.Play();
            Destroy(Sound);
        }
    }

    void Update()
    {
        
    }
}
