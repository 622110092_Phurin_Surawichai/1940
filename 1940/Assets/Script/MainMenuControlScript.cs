using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuControlScript : MonoBehaviour
{
    [SerializeField] Button startButton;
    [SerializeField] Button optionsButton;
    [SerializeField] Button exitButton;

   
    // Start is called before the first frame update
    void Start()
    {
        startButton.onClick.AddListener (delegate{StartButtonClick(startButton);});
        
        exitButton.onClick.AddListener (delegate{ExitButtonClick(exitButton);});
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void StartButtonClick(Button button) 
    {
         SceneManager.LoadScene("gameplayIguess");
    }
    
    
    
     public void ExitButtonClick(Button button) 
     {
         Application.Quit();
     }
    
}
