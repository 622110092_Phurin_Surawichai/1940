using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class MenuGameControl : MonoBehaviour
{
    
    [SerializeField] Button BackButton;
    [SerializeField] Button TryButton;
    [SerializeField] GameObject panel;
    [SerializeField] Text ScoreText;

    [SerializeField] Text Timer;

    [SerializeField] Text Score;
    
    public float timer;
    // Start is called before the first frame update
    void Start()
    {
        panel.SetActive(false);
        BackButton.onClick.AddListener (delegate{BackButtonClick(BackButton);});
        TryButton.onClick.AddListener (delegate{TryButtonClick(TryButton);});
        
    }

    // Update is called once per frame
    void Update()
    {
        timer = Timer.text.CompareTo(timer);
        if (timer <= 0)
        {
            panel.SetActive(true);
            Score = ScoreText;
        }
    }
    public void BackButtonClick(Button button) 
    {
        SceneManager.LoadScene("SceneMainMenu");
    }
    
    public void TryButtonClick(Button button)
    {
        SceneManager.LoadScene("gameplayIguess");
    }
    
}