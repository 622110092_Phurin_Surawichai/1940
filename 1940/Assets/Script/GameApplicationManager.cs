using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameApplicationManager : MonoBehaviour
{
    static protected GameApplicationManager singletonInstance = null;
    static public GameApplicationManager Instance 
    { 
        get {
         if (singletonInstance == null)
         {
             singletonInstance = GameObject.FindObjectOfType <GameApplicationManager >()
                ;
             GameObject container = new GameObject("GameApplicationManager");
             singletonInstance = container.AddComponent <GameApplicationManager >();
         }
         return singletonInstance;
         }
     }
    public string[] DIFFICULTY_LEVEL_NAMES = { "Easy", "Normal", "Hard", "Extreme" };
        
         public bool IsOptionMenuActive
     {
         get { return isOptionMenuActive; }
         set { isOptionMenuActive = value; }
         }
 protected bool isOptionMenuActive = false;

 public int DifficultyLevel
 {
     get { return difficultyLevel; }
     set { difficultyLevel = value; }
     }
 protected int difficultyLevel;

 public bool MusicEnabled
 {
     get { return isMusicEnabled; }
     set { isMusicEnabled = value; }
     }
 protected bool isMusicEnabled = true;

 public bool SFXEnabled
 {
     get { return isSFXEnabled; }
     set { isSFXEnabled = value; }
     }
 protected bool isSFXEnabled = true;

 public InputField Name
 {
     get { return name;}
     set { name = Name; }
 }

 protected InputField name;

 void Awake()
 {
     if (singletonInstance == null)
     {
         singletonInstance = this;
         DontDestroyOnLoad(this.gameObject);
     }
     else
     {
         if (this != singletonInstance)
         {
             Destroy(this.gameObject);
         }
     }
 }

 // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
