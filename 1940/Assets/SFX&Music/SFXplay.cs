using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXplay : MonoBehaviour
{
    [SerializeField] public AudioSource _SFX;
    [SerializeField] public float timer;
    private float time;
    [SerializeField] public float RandomStartTime;
    [SerializeField] public float RandomEndTime;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("PlaySFX", Random.Range(RandomStartTime, RandomEndTime), timer);
    }

    // Update is called once per frame
    void Update()
    {
        //time += Time.deltaTime;
    }
    public void PlaySFX()
    {
        //if(time>timer)
        //{
            _SFX.Play();
            
        //}
        //time = 0;
    }
}
