using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetSFX : MonoBehaviour
{
    
    [SerializeField] private AudioClip[] hitsound;
    [SerializeField] private AudioClip goalSFX;
    private AudioSource _AudioSource;
    // Start is called before the first frame update
    void Start()
    {
       _AudioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag=="Bullet")
        {
            PlayHitAudio();
        }
        else if (other.tag=="Ball")
        {
            PlayGoalAudio();
        }
    }
    private void PlayHitAudio()
    {
        
        // pick & play a random footstep sound from the array,
        // excluding sound at index 0
        int n = Random.Range(1, 2);
        _AudioSource.clip = hitsound[n];
        _AudioSource.PlayOneShot(_AudioSource.clip);
        // move picked sound to index 0 so it's not picked next time
        hitsound[n] = hitsound[0];
        hitsound[0] = _AudioSource.clip;
    }
    private void PlayGoalAudio()
    {

        // pick & play a random footstep sound from the array,
        // excluding sound at index 0
        
        _AudioSource.clip = goalSFX;
        _AudioSource.time = 6.5f;
        _AudioSource.PlayOneShot(_AudioSource.clip);
        // move picked sound to index 0 so it's not picked next time
        
        
    }
}
